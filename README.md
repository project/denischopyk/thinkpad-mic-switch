Данный фикс тестировался на ОС Astra Linux и ноутбуке Lenovo Thinkpad L430. Думаю также заработает на других дистрибутивах и с другими ноутбуками, но возможно придётся немного изменить скрипт.

И так, начнём! Для начала введём команду 

```bash
acpi_listen
```

При нажатии кнопки отключения микрофона появляется такое вот сообщение: 

```bash
dd@astra ~/D/l/thinkpad-mic-switch> acpi_listen
button/f20 F20 00000080 00000000 K
```

Ага, вот эта строка нам и нужна  **button/f20 F20 00000080 00000000 K**

Создаём файл для обработки события кнопки:

```bash
sudo nano /etc/acpi/events/lenovo-mutemic
```

```bash
event=button/f20 F20 00000080 00000000 K
action=/etc/acpi/lenovo-mutemic.sh
```

Создаём скрипт для управления состоянием микрофона:

```bash
sudo nano /etc/acpi/lenovo-mutemic.sh
```

```bash
#!/bin/bash
INPUT_DEVICE="'Capture'"
YOUR_USERNAME="имя юзера"
if amixer sget $INPUT_DEVICE,0 | grep '\[on\]' ; then
    amixer sset $INPUT_DEVICE,0 toggle
    echo "0 blink" > /proc/acpi/ibm/led
    su $YOUR_USERNAME -c 'DISPLAY=":0.0" notify-send -t 50 \
            -i microphone-sensitivity-muted-symbolic "Mic MUTED"'
else
    amixer sset $INPUT_DEVICE,0 toggle                       
    su $YOUR_USERNAME -c 'DISPLAY=":0.0" notify-send -t 50 \
            -i microphone-sensitivity-high-symbolic "Mic ON"'
    echo "0 on" > /proc/acpi/ibm/led 
fi
```

Чтобы узнать INPUT_DEVICE, нужно выполнить команду:

```bash
amixer scontrols
```

```bash
dd@astra ~/D/l/thinkpad-mic-switch> amixer scontrols
Simple mixer control 'Master',0
Simple mixer control 'Capture',0
```

Сохраняем скрипт, выдаём ему права на выполнение и перезапускаем ACPI:

```bash
sudo chmod +x /etc/acpi/lenovo-mutemic.sh
sudo service acpid restart
```

Проверяем работу и радуемся)


